using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ConsulAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("{id}")]
        [Authorize(Roles ="admin")]//请求受限,只有admin能访问
        public IActionResult Get(int id)
        {
            return Ok(new
            {
                code = "200",
                msg = "受限访问数据",
                data = "Good good Study,day day up"
            });
        }

        [HttpGet(Name = "GetWeatherForecast")]
        [AllowAnonymous]//允许匿名访问
        public IActionResult Get()
        {
            //Task.Delay(5000).Wait(); //程序停留时间，超过就报错

            var res= Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();

            return Ok(new
            {
                code = "200",
                msg = "success",
                dTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                datas=res
            });
        }
    }
}
