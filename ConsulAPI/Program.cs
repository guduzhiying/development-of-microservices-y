using ConsulAPI;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Configuration.AddEnvironmentVariables();//该微服务的配置把环境变量的值加进去

//方法三：添加健康检测模块(普遍方法)。下面还要补充
builder.Services.AddHealthChecks();


builder.Services.AddAuthentication("Bearer").AddJwtBearer("Bearer", options =>
{
    //配置验证服务器是哪个
    options.Authority = "http://localhost:7777";
    //不用https
    options.RequireHttpsMetadata=false;
    //配置一些验证的参数的特性
    options.TokenValidationParameters =
          new Microsoft.IdentityModel.Tokens.TokenValidationParameters
          {
              //不要验证aud
              ValidateAudience = false
          };
});

//IOC思想，不需要手工new一个类
//自动实例化
builder.Services.AddHostedService<ConsulRegister>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();



//方法一：自己做个controll
//方法二：快速建立一个健康接口 Minimal API
/*app.MapGet("/health", () => "I am ok");*/

//方法三补充
app.UseHealthChecks("/health");

app.MapControllers();

app.Run();
