﻿namespace 依赖注入_DI_与IOC.Controllers
{
    public class OracleDataService : IDataService
    {
        public string ID=Guid.NewGuid().ToString();
        public void GetDatas()
        {
            Console.WriteLine("从Oracle取数据");
        }
    }
}
