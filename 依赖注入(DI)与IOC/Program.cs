using System.Reflection;
using 依赖注入_DI_与IOC.Controllers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

//DI
//实现依赖注入,只要遇到 IDataService 自动注入 SQLServerDataService 的数据
builder.Services.AddTransient<IDataService,OracleDataService>();
//同理Oracle
//builder.Services.AddTransient<IDataService, OracleDataService>();



// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

//生成注释文字
builder.Services.AddSwaggerGen(a=>
{
    //对v1版本做说明
    a.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
    {
        Version = "v1",
        Title = "天气预报API服务说明",//文字描述
        Description="软件工程专业开发"//开发者
    });

    //增加注释文档
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    a.IncludeXmlComments(xmlPath);
});

//增加注释文档
var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
