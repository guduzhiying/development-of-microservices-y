﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SecondAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET: api/<ValuesController1>
        [HttpGet]
        /*public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }*/
        /*public IEnumerable<string> Get1([FromQuery] string value)
        {
            return new string[] { "value1", "value2" };
        }*/
        /* public IEnumerable<string> Get1([FromHeader] string cat)
         {
             return new string[] { "value1", "value2" };
         }*/
        public IEnumerable<string> Get1([FromForm] string name, [FromForm] string p)
        {
            return new string[] { "FromForm测试" };
        }

        // GET api/<ValuesController1>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ValuesController1>
        [HttpPost]//推送（添加）
        public void Post([FromBody] string value)//[FromBody] 数据0需从正文传数据
        {
            string x=value;
        }

        // PUT api/<ValuesController1>/5
        [HttpPut("{id}")]//修改(某个id)信息
        public void Put(int id, [FromBody] string value)
        {
            string x =value;
        }

        // DELETE api/<ValuesController1>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            string x=id.ToString();
        }
    }
}
