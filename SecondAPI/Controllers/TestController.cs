﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SecondAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        // GET: api/<TestController>
        /*[HttpGet]
        public IActionResult Get()
        {
            // return new string[] { "value1", "value2" };
            //return Ok("hello");//返回响应的状态码为 200,Ok可包含所有类型
            //return BadRequest();//坏请求,数据不合法，400
            //return NotFound();//请求不存在，404

            //返回规范格式的内容
            return Ok(new{
                code=200,
                msg="操作成功",
                data= new string[] { "value1", "value2" }
        });
        }*/

        [HttpGet]
        public async Task<IActionResult> Get1()
        {
            var res = await Task.Run(() => ReadDatas());
            return Ok(new
            {
                code = 200,
                msg = "success",
                data = res
            }) ;
        }
        private string ReadDatas()
        {
            //从别的服务器获取数据
            Task.Delay(3000).Wait();//需要3秒取到数据
            return "这是响应的数据";
        }

    }
}
