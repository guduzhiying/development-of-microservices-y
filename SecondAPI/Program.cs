var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

/*builder.Services.AddControllers();//添加使用控制器*/
builder.Services.AddControllers().AddXmlSerializerFormatters();//API可提供XML数据
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
