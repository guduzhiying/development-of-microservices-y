﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using Microsoft.AspNetCore.Identity;
using System.Runtime.Intrinsics.Arm;

namespace AuthCenter
{
    public class Config
    {
        /// <summary>
        /// 定义可以访问的客户端以及登录类型
        /// </summary>
        /// <returns></returns>
        public static List<Client> GetClients()
        {
            return new List<Client>()
            {
                new Client()
                {
                    //客户端凭证模式
                    AllowedGrantTypes=GrantTypes.ClientCredentials,
                    //客户端标识
                    ClientId="mobile_app",
                    //客户端密码
                    ClientSecrets=new[]{new  Secret("mm_test".Sha256())},
                    //客户端权限
                    AllowedScopes=new[]{ "WeatherAPI.full_access" },
                    //JWT Token有效期
                    AccessTokenLifetime=60
                },
                new Client()
                {
                    //验证模式采用用户名密码方式
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    //客户端标识
                    ClientId="wx_app",
                    //客户端密码
                    ClientSecrets=new[]{new  Secret("mm_test".Sha256())},
                    //客户端权限
                    AllowedScopes=new[]{ "WeatherAPI.full_access" },
                    //JWT Token有效期
                    AccessTokenLifetime=60
                }
            };
        }

        /// <summary>
        /// 定义客户端可以访问哪些API
        /// </summary>
        /// <returns></returns>
        public static List<ApiResource> GetApiResources()
        {
            return new List<ApiResource>()
            {
                new ApiResource("WeatherAPI","天气访问API"),
                new ApiResource("TicketAPI","购票API")
            };
        }

        /// <summary>
        /// 定义用户权限的种类
        /// </summary>
        /// <returns></returns>
        public static List<ApiScope>GetApiScopes()
        {
            return new List<ApiScope>()
            {
                new ApiScope("WeatherAPI.full_access","Weather API访问的所有权限"),
                new ApiScope("WeatherAPI.read_only","Weather API访问的只读权限"),
            };
        }

        public static List<TestUser> GetTestUsers()
        {
            return new List<TestUser>()
            {
                new TestUser() {Username="lisi",Password="1234",SubjectId="001"},//用户名，密码，主键
                new TestUser() {Username="wang",Password="555",SubjectId="002"},
            };
        }
    }
}
