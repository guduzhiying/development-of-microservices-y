using AuthCenter;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//配置ids4服务器
builder.Services.AddIdentityServer()
    .AddDeveloperSigningCredential()
    .AddInMemoryClients(Config.GetClients())
    .AddInMemoryApiResources(Config.GetApiResources())
    .AddInMemoryApiScopes(Config.GetApiScopes())
    //使用自己的密码验证模式
    .AddResourceOwnerValidator<CustomResourceOwnerPasswordValidator>()
    //把自定义claim声明加入JWT
    .AddProfileService<CustomeProfileService>();
/*.AddTestUsers(Config.GetTestUsers());//测试用户*/
//所有IdentityServer中间件,添加开发者发明证书,后三行添加权限



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseIdentityServer();//添加对应使用
//app.UseAuthorization();

app.MapControllers();

app.Run();
