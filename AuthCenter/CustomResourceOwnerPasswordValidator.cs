﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using System.Security.Claims;

namespace AuthCenter
{//使用自己的正式用户
    public class CustomResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            //数据库读写
            //略

            //自定义密码验证--->自定义验证器
            if (context.UserName =="caoyang"&&context.Password=="666")
            {
                List<Claim> claims = new List<Claim>()
                {
                   new Claim("role","admin"),
                   new Claim("qq","123456"),
                   new Claim("email", "123456@qq.com"),
                   new Claim("age", "46")
                };
                context.Result=new GrantValidationResult("student001","custom",claims);
            }
            else
            {
                context.Result = new 
                    GrantValidationResult(TokenRequestErrors.InvalidGrant, "用户名或密码错误");
            }
            return Task.CompletedTask;
        }
    }
}
