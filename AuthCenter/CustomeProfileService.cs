﻿using IdentityServer4.Models;
using IdentityServer4.Services;

namespace AuthCenter
{
    public class CustomeProfileService : IProfileService
    {//
        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var claims=context.Subject.Claims;
            context.IssuedClaims = claims.ToList();
            return Task.CompletedTask;
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;
            return Task.CompletedTask;
        }
    }
}
