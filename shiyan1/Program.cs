using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Net;
using shiyan1;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//方法三：添加健康检测模块(普遍方法)。下面还要补充
builder.Services.AddHealthChecks();

//IOC思想，不需要手工new一个类
//自动实例化
builder.Services.AddHostedService<ConsulRegister>();

//更改返回的错误信息
builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.InvalidModelStateResponseFactory = (context) =>
    {
        return new JsonResult(new
        {
            code = HttpStatusCode.BadRequest,
            msg = "数据验证未通过",
            error = context.ModelState
        });
    };
});
//跨域配置
builder.Services.AddCors(opt =>
{
    opt.AddPolicy("AllowCors", builder => {
        builder.AllowAnyOrigin().AllowAnyHeader().WithMethods("GET,POST,PUT,DELETE,OPTION");
        });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
//使用跨域配置
app.UseCors("AllowCors");

app.UseHttpsRedirection();

app.UseAuthorization();

//方法一：自己做个controll
//方法二：快速建立一个健康接口 Minimal API
/*app.MapGet("/health", () => "I am ok");*/

//方法三补充
app.UseHealthChecks("/health");

app.UseAuthorization();

app.MapControllers();

app.Run();
