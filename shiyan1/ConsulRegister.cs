﻿using Consul;

namespace shiyan1
{
    public class ConsulRegister : IHostedService//该接口用来实现：程序开始时干活，结束是关闭
    {

        ConsulClient _client;
        private string _serviceID = Guid.NewGuid().ToString();
        private IConfiguration _configuration;
        public ConsulRegister(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                AgentServiceRegistration agentServiceRegistration = new AgentServiceRegistration()
                {
                    Address = "http://localhost",//webAPIip地址
                    Port = 5555,//webAPI端口号
                    ID = _serviceID,//服务的ID
                    Name = "StudentService",
                    Check = new AgentServiceCheck()
                    {
                        Interval = TimeSpan.FromSeconds(5),//每隔5秒检测1次健康
                        HTTP = "http://localhost:5555/health",//健康检测地址
                    }
                };
                /* _client = new ConsulClient();*///实例化ConsulClient
                                                  //实例化，并且定义数据中心地址
                _client = new ConsulClient(new ConsulClientConfiguration()
                {
                    Address = new Uri(_configuration.GetConnectionString("Consul_Address"))
                });

                await _client.Agent.ServiceRegister(agentServiceRegistration);
                Console.WriteLine("去consul自动注册");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString);
            }
            /*return Task.CompletedTask;*/
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _client.Agent.ServiceDeregister(_serviceID);
            Console.WriteLine("从consul自动注销");
            /*return Task.CompletedTask;*/
        }
    }
}
