﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;
using shiyan1.Models;

namespace shiyan1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private static List<Student> students = new List<Student>//静态方法
        {
            new Student{Id=1,Name="张三",Score=93},
            new Student{Id=2,Name="李四",Score=92},
            new Student{Id=3,Name="王五",Score=97}
        };


        [HttpGet]//获取/查找
        public IActionResult GetAllStudents()
        {
            return Ok(new
            {
                code = 200,
                msg = "success",
                data = students
            });
        }
        [HttpDelete("{id}")]//删
        public IActionResult DeleteStudentById(int id)
        {
            Student? rst = students.Find(x => x.Id == id);
            if (rst != null)
            {
                students.Remove(rst);
                return Ok(new
                {
                    code = 200,
                    msg = "success",
                    data = students
                });
            }
            else
            {
                return Ok(new
                {
                    code = 404,
                    msg = "没有这个学生"
                });
            }

        }
        [HttpPost]//增
        public IActionResult AddStudent([FromBody] Student stu) //从正文里取
        {
            students.Add(stu);
            return Ok(new
            {
                code = 200,
                msg = "success",
                data = students
            });
        }

        [HttpPut("{id}")]//改
        public IActionResult ChangeStuScore(int id, [FromBody] Student newStu)
        {
            Student? rst = students.Find(x => x.Id == id);
            if (rst != null)
            {
                rst.Name = newStu.Name;
                rst.Score = newStu.Score;
                return Ok(new
                {
                    code = 200,
                    msg = "success"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 404,
                    msg = "找不到该学生"
                });
            }
        }
        /*public IActionResult ChangeStudent(int id, [FromBody] Student newStu)
        {
            Student? rst = students.Find(x => x.Id == id);
            if (rst != null)
            {
                rst.Name = newStu.Name;
                rst.Score = newStu.Score;
                return Ok(new
                {
                    code = 200,
                    msg = "success"
                });
            }
            else
            {
                return Ok(new
                {
                    code = 404,
                    msg = "找不到该学生"
                });
            }
        }*/
        //根据id查找某个学生的数据  Get
        [HttpGet("{id:int}")]//路由约束
        public IActionResult GetStuById(int id)
        {
            Student? rst = students.Find(x => x.Id == id);
            if (rst != null)
            {
                return Ok(new
                {
                    code = 200,
                    msg = "success",
                    data = rst
                });
            }
            else
            {
                return Ok(new
                {
                    code = 404,
                    msg = "没有找到对应的学生"
                });
            }
        }
        //根据Name查找某个学生的数据  Get
        //地址不能重复
        /*[HttpGet("{name}")]
        public IActionResult GetAllStudents([FromQuery]string name = "*")
        {
            if (name == "*")
            {
                return Ok(new
                {
                    code = 200,
                    msg = "success",
                    data = students
                });
            }
            else
            {
                Student? stu = students.Find(x => x.Name == name);
                return Ok(new
                {
                    code = 200,
                    msg = "success",
                    data = stu
                });
            }
        }*/


    }
}
