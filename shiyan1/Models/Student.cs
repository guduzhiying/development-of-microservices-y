﻿using System.ComponentModel.DataAnnotations;

namespace shiyan1.Models
{  //模型验证:对类的输入值进行验证
    public class Student
    {
        [Required]//必须写
        public int Id { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        [Range(0,100,ErrorMessage ="分数必须在0~100之间")]//限制范围,ErrorMessage用来显示错误信息
        public int Score { get; set;}


      /*  [EmailAddress]//邮箱格式
        public string? Email { get; set; }
        [RegularExpression("")]
        public string CreditID {  get; set; }

        public string pass {  get; set; }
        [Compare("pass")]
        public string ConfirmPass {  get; set; }*/
    }

}
