﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FirstAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new[] { "value1","value2" };
        }
        [HttpGet("{id}")]
        public string Get1()
        {
            return "value";
        }
        [HttpPut("{id}")]
        public string Get2(int id)
        {
            return "value";
        }
    }
}
