using Ocelot.Cache.CacheManager;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using Ocelot.Provider.Polly;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//使用配置文件，加入Ocelot网关
ConfigurationBuilder configuration=new ConfigurationBuilder();
configuration.AddJsonFile("Configs/config7.json", false);
//config1.json路由转移
//config2.json增加缓存
//加入豹子-->中间件
builder.Services
    .AddOcelot(configuration.Build()).AddPolly();
    /*.AddConsul()//暂时不用
    .AddCacheManager(x=>x.WithDictionaryHandle());*/



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


//使用中间件
app.UseOcelot();

app.UseHttpsRedirection();

app.UseAuthorization();


app.Run();
